#include "allDayZone1.h"

AllDayZone1::AllDayZone1() : TravelPass(){

	setLength("All Day");
	setZones("Zone 1");
	setCost(4.90);

}

AllDayZone1::AllDayZone1(string theLength, string theZones, float theCost)
				: TravelPass(theLength, theZones, theCost){ }

AllDayZone1::~AllDayZone1(){ }

// This function allows you to set the instance variables manually,
// or override the existing values
void AllDayZone1::input(){

	string newLength, newZone; 
	float newCost;

	cout << "\nEnter a new length: ";
	getline(cin, newLength);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new zone: ";
	getline(cin, newZone);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new cost: $";
	cin >> newCost;
	clearInputBuffer();
	cout << "\n";

	setLength(newLength);
	setZones(newZone);
	setCost(newCost);

}

void AllDayZone1::print(){

	cout << "\nYou purchased " << length << " pass for " << zones << ", costing $" << cost << endl;

}