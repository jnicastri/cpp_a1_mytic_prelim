#include "allDayZone1And2.h"

AllDayZone1And2::AllDayZone1And2() : TravelPass(){

	setLength("All Day");
	setZones("Zones 1 and 2");
	setCost(6.80);

}

AllDayZone1And2::AllDayZone1And2(string theLength, string theZones, float theCost)
					: TravelPass(theLength, theZones, theCost){ }


AllDayZone1And2::~AllDayZone1And2(){ }

// This function allows you to set the instance variables manually,
// or override the existing values
void AllDayZone1And2::input(){

	string newLength, newZone;
	float newCost;

	cout << "\nEnter a new length: ";
	getline(cin, newLength);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new zone: ";
	getline(cin, newZone);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new cost: $";
	cin >> newCost;
	clearInputBuffer();
	cout << "\n";

	setLength(newLength);
	setZones(newZone);
	setCost(newCost);

}

void AllDayZone1And2::print(){

	cout << "\nYou purchased " << length << " pass for " << zones << ", costing $" << cost << endl;

}