//
//myTic.h
//
// MyTic class
//

#ifndef __MYTIC_H__
#define __MYTIC_H__

#include <vector>
#include <string>
#include "travelPass.h"
/*TO DO  REQUIRED HEADER FILES AND NAMESPACES*/

class MyTic
{
private:
  float credit; 
  float limit;
  vector<TravelPass*> travelPasses;
  
public:
  MyTic();
  MyTic(float theCredit, float theLimit);
   ~MyTic() ; 
  void input() ;   // Data input for a MyTic object
  void print() ;  // Data output for a MyTic object
  void printPurchases();

  float getCredit() const;   //Note the use of const
  float getLimit() const;

  void addCredit(float addAmount);
  void addPurchase(TravelPass* purchaseItem);

};

#endif
