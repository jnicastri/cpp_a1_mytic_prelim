#include "utility1.h"

////////////////////////////////////////////
void clearInputBuffer()     // function to clear the input buffer
{
   char symbol;
   do
   {
      cin.get(symbol);
   }
   while(symbol != '\n');
}

void printMainMenu(){

	cout << "\nChoose an option:\n"
		<< "1. Buy a travel pass\n"
		<< "2. Charge my MyTic\n"
		<< "3. Show remaining credit\n"
		<< "4. Print purchases\n"
		<< "0. Quit\n"
		<< "Please make a selection: ";
	
}
