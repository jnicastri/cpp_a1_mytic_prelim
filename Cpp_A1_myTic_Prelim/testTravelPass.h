//
// testPropertyA.h
//
// header file for the driver program
//

#ifndef __TESTTRAVELPASS_H__
#define __TESTTRAVELPASS_H__

#include<iostream>
#include<string>
#include "myTic.h"

#include "travelPass.h"

#include "twoHoursZone1.h"
#include "twoHoursZone1And2.h"
#include "allDayZone1.h"
#include "allDayZone1And2.h"
#define MAX_TRAVELPASSES 100

using namespace std;

typedef MyTic* MyTicPtr;

void buyTravelPass(MyTicPtr myTicket);
void chargeMyTic(MyTicPtr myTicket);
#endif
