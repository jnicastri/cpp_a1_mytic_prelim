#include "twoHoursZone1And2.h"

TwoHoursZone1And2::TwoHoursZone1And2() : TravelPass(){

	setLength("2 Hour");
	setZones("Zone 1 and 2");
	setCost(3.50);

}

TwoHoursZone1And2::TwoHoursZone1And2(string theLength, string theZones, float theCost)
						: TravelPass(theLength, theZones, theCost){ }

TwoHoursZone1And2::~TwoHoursZone1And2(){ }

// This function allows you to set the instance variables manually,
// or override the existing values
void TwoHoursZone1And2::input(){

	string newLength, newZone;
	float newCost;

	cout << "\nEnter a new length: ";
	getline(cin, newLength);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new zone: ";
	getline(cin, newZone);
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new cost: $";
	cin >> newCost;
	clearInputBuffer();
	cout << "\n";

	setLength(newLength);
	setZones(newZone);
	setCost(newCost);

}

void TwoHoursZone1And2::print(){

	cout << "\nYou purchased " << length << " pass for " << zones << ", costing $" << cost << endl;

}