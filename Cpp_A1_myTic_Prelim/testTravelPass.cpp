// testTravelPass.cpp for CPT 323 Assignment 1 SP3 2014
//
//
// CPT323 2014  assignment 1



#include "testTravelPass.h"

/////////////////////////////////////////////////////
int main()
{

	MyTicPtr myTicket;
	myTicket	= new MyTic();
	int userSelectNum = -1;

	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);

	cout << "Welcome to MyTic!" << endl;

	do{
		printMainMenu();
		
		cin >> userSelectNum;
		clearInputBuffer();

		switch (userSelectNum){
			case 1: //Buy a travel pass
				buyTravelPass(myTicket);
				break;
			case 2: //Charge MyTic
				chargeMyTic(myTicket);
				break;
			case 3: //Show remaining credit
				cout << "\nYour credit = $" << myTicket->getCredit() << endl;
				break;
			case 4: //Print purchases
				myTicket->printPurchases();
				break;
			case 0:
				break;
			default:
				cout << "\nSorry, that is an invalid option!" << endl;
				break;
		}

	} while (userSelectNum != 0);

	cout << "\nGoodbye!";

	delete myTicket;

return 0;
}


//Handles the purchasing of a TravelPass
void buyTravelPass(MyTicPtr myTicket){
	
	char periodSelection, zoneSelection;

	cout << "\nWhat time period:\n"
		<< "a) 2 Hours\n"
		<< "b) All Day\n"
		<< "c) Cancel\n"
		<< "Your selection: ";
	cin >> periodSelection;
	clearInputBuffer();

	if (periodSelection == 'c' || periodSelection == 'C')
		return;

	cout << "\n\nWhich zone:\n"
		<< "a) Zone 1\n"
		<< "b) Zones 1 and 2\n"
		<< "c) Cancel\n"
		<< "Your selection: ";
	cin >> zoneSelection;

	if (zoneSelection == 'c' || zoneSelection == 'C')
		return;

	if ((periodSelection == 'a' || periodSelection == 'A') && (zoneSelection == 'a' || zoneSelection == 'A')){
		//2 Hours Zone 1 TravelPass
		TwoHoursZone1 *pass = new TwoHoursZone1();

		if ((myTicket->getCredit() - pass->getCost()) < 0){
			cout << "\nSorry, you don't have enough credit for that selection\n";
			delete pass;
			return;
		}

		myTicket->addPurchase(pass);
		pass->print();

	}
	else if ((periodSelection == 'a' || periodSelection == 'A') && (zoneSelection == 'b' || zoneSelection == 'B')){
		//2 Hours Zones 1 and 2 TravelPass
		TwoHoursZone1And2 *pass = new TwoHoursZone1And2();

		if ((myTicket->getCredit() - pass->getCost()) < 0){
			cout << "\nSorry, you don't have enough credit for that selection\n";
			delete pass;
			return;
		}

		myTicket->addPurchase(pass);
		pass->print();

	}
	else if ((periodSelection == 'b' || periodSelection == 'B') && (zoneSelection == 'a' || zoneSelection == 'A')){
		//Add Day Zone 1 TravelPass
		AllDayZone1 *pass = new AllDayZone1();

		if ((myTicket->getCredit() - pass->getCost()) < 0){
			cout << "\nSorry, you don't have enough credit for that selection\n";
			delete pass;
			return;
		}

		myTicket->addPurchase(pass);
		pass->print();

	}
	else if ((periodSelection == 'b' || periodSelection == 'B') && (zoneSelection == 'b' || zoneSelection == 'B')){
		//Add Day Zones 1 and 2 Travel Pass
		AllDayZone1And2 *pass = new AllDayZone1And2();

		if ((myTicket->getCredit() - pass->getCost()) < 0){
			cout << "\nSorry, you don't have enough credit for that selection\n";
			delete pass;
			return;
		}

		myTicket->addPurchase(pass);
		pass->print();

	}

	cout << "\nYour remaining credit is $" << myTicket->getCredit() << endl;
}

//Adds credit to the myTic instance
void chargeMyTic(MyTicPtr myTicket){
	
	int userAmount;
	bool success = false;

	do{
		cout << "\nHow much do you want to add: ";
		cin >> userAmount;
		clearInputBuffer();

		if ((myTicket->getCredit() + userAmount) > 100){
			cout << "\nSorry, the max amount of credit allowed is $" << myTicket->getLimit() << endl;
		}
		else if ((userAmount % 5) != 0){
			cout << "\nSorry, you can only add multiples of $5.00" << endl;
		}
		else{
			myTicket->addCredit(userAmount);
			cout << "\nYour credit = $" << myTicket->getCredit() << "\n";
			success = true;
		}
	} while (!success);

}
