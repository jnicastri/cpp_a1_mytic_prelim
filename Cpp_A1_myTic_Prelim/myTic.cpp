#include "myTic.h"

MyTic::MyTic() : credit(0), limit(100) { }

MyTic::MyTic(float theCredit, float theLimit)
: credit(theCredit), limit(theLimit) { }

MyTic::~MyTic(){

	int vSize = travelPasses.size();

	for (int i = 0; i < vSize; i++){
		delete travelPasses[i];
	}
}

// Allows you to set this MyTics instances data members manually
// or override the data that may be pre-existing.
// Note: this is not actually called anywhere in this application!
void MyTic::input(){
	
	float newLimit, newCredit;

	cout << "\nEnter a new credit amount: $";
	cin >> newCredit;
	clearInputBuffer();
	cout << "\n";

	cout << "\nEnter a new limit amount: $";
	cin >> newLimit;
	clearInputBuffer();
	cout << "\n";

	credit = newCredit;
	limit = newLimit;

}

// Simply prints out the values of this instances variables
// Note: this is not actually called anywhere in this application!
void MyTic::print(){
	
	cout << "Credit = $" << getCredit() << ", Limit = $" << getLimit() << endl;

}

void MyTic::printPurchases(){

	int vSize = travelPasses.size();
	cout << "\nPurchases:\n";

	if (vSize != 0){
		for (int i = 0; i < vSize; i++){
			cout << "Purchased " << travelPasses[i]->getLength()
				<< " pass for " << travelPasses[i]->getZones()
				<< ", costing $" << travelPasses[i]->getCost()
				<< endl;
		}
	}
	else{
		cout << "You have made no purchases yet" << endl;
	}

}

float MyTic::getCredit() const{

	return credit;

}

float MyTic::getLimit() const{

	return limit;

}

void MyTic::addCredit(float addAmount){

	credit += addAmount;

}

void MyTic::addPurchase(TravelPass* purchaseItem){

	credit -= purchaseItem->getCost();

	travelPasses.push_back(purchaseItem);

}
